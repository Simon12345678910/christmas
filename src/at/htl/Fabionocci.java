package at.htl;

import javax.swing.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class Fabionocci{

    public static void main(String[] args) throws XMASException {

        int s = Fabionocci();
        System.out.println(""+s);

    }

    public static int Fabionocci(){

        int num1 = 1;
        int num2 = 1;
        int res = 0;
        int solution = 0;

        while (res < 10){

            res = num1+num2;
            num2 = num1;
            num1 = res;

            if(res%2 ==0){

                solution += res;

            }

        }

        return solution;

    }

    public static int solve(String s) throws XMASException {

        int x;
        try {
            x = Integer.parseInt(s);
            if (x == Fabionocci()) {

                System.out.println("Yey, you did it!!!");
                return x;

            }
            else{
                return 404;
            }

        } catch (NumberFormatException e) {
            throw new XMASException();
        }

    }
}