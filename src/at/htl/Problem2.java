package at.htl;

import java.math.BigInteger;

public class Problem2 {

    public static void main(String[] args) {

        System.out.println("What is the Power digit sum of 2^1000");
        System.out.println(calculate());
    }
    public static int calculate(){

        int a = 1000;
        int b = 2;
        BigInteger erg = new BigInteger(String.valueOf(b));
        BigInteger l =  erg.pow(a);

        String s = String.valueOf(l);
        int length = s.length();
        int sum = 0;
        int add = 0;

        for(int i = 0; i < length;i++){

            add = s.charAt(i)-48;
            sum += add;


        }
        return sum;

    }

    public static int solve(String x) throws XMASException{

       int z;

        try {
        z = Integer.parseInt(x);
            if (z == calculate()) {

            System.out.println("Yey, you solved it");
            return z;

            }
            else{

            return 404;

            }

        } catch (NumberFormatException e) {
            throw new XMASException();
        }

    }

    }
