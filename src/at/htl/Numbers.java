package at.htl;

import java.math.BigInteger;

public class Numbers {

    public static void main(String[] args) {

        System.out.println(num());
    }

    public static String num(){

        BigInteger l;
        BigInteger sum = new BigInteger("0");


        for(int i = 0; i < 1000;i++){

            BigInteger z = new BigInteger(""+i);
            l = z.pow(i);
            sum = sum.add(l);
        }


        String s = sum.toString();
        String dig = "";

        for(int i = s.length()-10; i < s.length();i++){

            dig += ""+s.charAt(i);

        }

        return dig;

    }

    public static String solve (String z) throws XMASException{

        long x;

        try {
            x = Long.parseLong(z);
            z = ""+x;

            if (num().equalsIgnoreCase(z)) {

            System.out.println("Yey, you solved it");
            return z;
            }
            else{
                return "404";
            }

        } catch (NumberFormatException e) {
            throw new XMASException();
        }

    }



}
