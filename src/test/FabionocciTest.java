package test;

import at.htl.Fabionocci;
import at.htl.XMASException;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class FabionocciTest {

    @org.junit.jupiter.api.Test
    public void solve() throws XMASException {

     assertEquals(404 , Fabionocci.solve("9"));


    }

    @org.junit.jupiter.api.Test

    public void solve2() throws XMASException{

        assertEquals(10, Fabionocci.solve("ala"));

    }

    @org.junit.jupiter.api.Test

    public void solve3() throws XMASException{

        assertEquals(10, Fabionocci.solve("10"));

    }



}