package test;

import at.htl.Numbers;
import at.htl.XMASException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumbersTest {

    @Test
    public void num() throws XMASException {

    assertEquals("9110846701", Numbers.solve("9110846701"));

    }

    @Test
    public void num2() throws XMASException {

    assertEquals("9110846701", Numbers.solve("a"));

    }

    @Test
    public void num3() throws XMASException {

    assertEquals("404", Numbers.solve("91"));

    }


}