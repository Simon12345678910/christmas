package test;

import at.htl.Problem30;
import at.htl.XMASException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Problem30Test {

    @Test
    public void solve() throws XMASException {

        assertEquals(443839, Problem30.solve("443839"));

    }

    @Test
    public void solve2() throws XMASException {

        assertEquals(443839, Problem30.solve("a"));

    }
    @Test
    public void solve3() throws XMASException {

        assertEquals(404, Problem30.solve("443"));

    }
}