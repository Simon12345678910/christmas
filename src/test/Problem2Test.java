package test;

import at.htl.Problem2;
import at.htl.XMASException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Problem2Test {

    @Test
    public void calculate() throws XMASException {

assertEquals(404, Problem2.solve("10"));

    }

    @Test
    public void calculate2() throws XMASException {

        assertEquals(1366, Problem2.solve("1366"));

    }

    @Test
    public void calculate3() throws XMASException {

        assertEquals(404, Problem2.solve("a"));

    }

}